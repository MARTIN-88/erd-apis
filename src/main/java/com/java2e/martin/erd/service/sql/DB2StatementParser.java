package com.java2e.martin.erd.service.sql;

import cn.fisok.pdman.dbreverse.Entity;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.db2.ast.stmt.DB2CreateTableStatement;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xiaobai
 *
 * DB2语句解析器
 */
@Component("db2StatementParser")
public class DB2StatementParser extends AbstractStatementParser {

    @Override
    public void parseStatement(List<SQLStatement> stmtList, List<Entity> entities) {
        for (SQLStatement sqlStatement : stmtList) {
            // 非DB2建表语句则直接跳过
            if (!(sqlStatement instanceof DB2CreateTableStatement)) {
                continue;
            }

            DB2CreateTableStatement createTableStatement = (DB2CreateTableStatement)sqlStatement;
            entities.add(buildEntity(createTableStatement));
        }
    }

}
