package com.java2e.martin.erd.service.sql;

import cn.fisok.pdman.dbreverse.Entity;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLCreateTableStatement;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xiaobai
 *
 * 基础语句解析器
 */
@Component("basicStatementParser")
public class BasicStatementParser extends AbstractStatementParser {

    @Override
    public void parseStatement(List<SQLStatement> stmtList, List<Entity> entities) {
        for (SQLStatement sqlStatement : stmtList) {
            // 非建表语句则直接跳过
            if (!(sqlStatement instanceof SQLCreateTableStatement)) {
                continue;
            }

            SQLCreateTableStatement createTableStatement = (SQLCreateTableStatement)sqlStatement;
            entities.add(buildEntity(createTableStatement));
        }
    }

}
