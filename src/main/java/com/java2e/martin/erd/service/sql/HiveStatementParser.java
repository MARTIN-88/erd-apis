package com.java2e.martin.erd.service.sql;

import cn.fisok.pdman.dbreverse.Entity;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.hive.stmt.HiveCreateTableStatement;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xiaobai
 *
 * Hive语句解析器
 */
@Component("hiveStatementParser")
public class HiveStatementParser extends AbstractStatementParser {

    @Override
    public void parseStatement(List<SQLStatement> stmtList, List<Entity> entities) {
        for (SQLStatement sqlStatement : stmtList) {
            // 非Hive建表语句则直接跳过
            if (!(sqlStatement instanceof HiveCreateTableStatement)) {
                continue;
            }

            HiveCreateTableStatement createTableStatement = (HiveCreateTableStatement)sqlStatement;
            entities.add(buildEntity(createTableStatement));
        }
    }

}
