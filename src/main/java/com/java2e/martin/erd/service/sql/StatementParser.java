package com.java2e.martin.erd.service.sql;

import cn.fisok.pdman.dbreverse.Entity;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLCreateTableStatement;

import java.util.List;

/**
 * @author xiaobai
 */
public interface StatementParser {

    /**
     * parseStatement
     *
     * @param stmtList stmtList
     * @param entities entities
     */
    void parseStatement(List<SQLStatement> stmtList, List<Entity> entities);

    /**
     * buildEntity
     *
     * @param createTableStatement createTableStatement
     * @return Entity
     */
    Entity buildEntity(SQLCreateTableStatement createTableStatement);

}
