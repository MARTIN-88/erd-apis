package com.java2e.martin.erd.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author xiaobai
 */
@Slf4j
public class FileUtil {

    public static String fileToContent(MultipartFile file) {
        StringBuilder content = new StringBuilder();
        try (InputStream inputStream = file.getInputStream();
             BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                content.append(String.format("%s\n", line));
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return content.toString();
    }

}
